import {Component, OnInit} from '@angular/core';
import {MessageService} from './message.service';
import Message from './message.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  username = '';
  author = '';
  message = '';

  constructor(private messageService: MessageService) {
  }

  setUsername() {
    this.author = this.username;
    this.username = '';
  }

  send() {
    this.messageService.sendMessageRest(new Message(this.author, this.message))
      .subscribe(() => this.message = '');
  }
}
