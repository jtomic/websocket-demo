import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Message from './message.model';
import {Client} from '@stomp/stompjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private socket: Client;
  public messages: Message[] = [];

  constructor(private http: HttpClient) {
    this.socket = new Client({brokerURL: 'ws://localhost:8080/websocket'});
    this.socket.onConnect = (() => {
      this.socket.subscribe('/topic/messages', (msg) => {
        this.messages.push(JSON.parse(msg.body));
      });
    });
    this.socket.activate();
  }

  public sendMessageRest(message: Message) {
    return this.http.post('http://localhost:8080/rest-message', message);
  }
}
