package hr.jtomic.websocketdemoback.api;

import hr.jtomic.websocketdemoback.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class MessageController {

    @Autowired
    private SimpMessagingTemplate template;

    private List<Message> messageList = new ArrayList<>();

    @MessageMapping("/socket-message")
    @SendTo("/topic/messages")
    public Message greetingSocket(Message message) throws Exception {
        messageList.add(message);
        return message;
    }

    @PostMapping("/rest-message")
    public Message greetingRest(@RequestBody Message message) throws Exception {
        messageList.add(message);
        template.convertAndSend("/topic/messages", message);
        return message;
    }
}
