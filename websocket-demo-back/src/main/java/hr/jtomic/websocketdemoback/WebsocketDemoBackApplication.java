package hr.jtomic.websocketdemoback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebsocketDemoBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebsocketDemoBackApplication.class, args);
	}

}
